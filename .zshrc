# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
  export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="agnoster"

# Set list of themes to load
# Setting this variable when ZSH_THEME=random
# cause zsh load theme from this variable instead of
# looking in ~/.oh-my-zsh/themes/
# An empty array have no effect
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
ZSH_CUSTOM=~/.zshcustom

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git
  autopep8
  mosh
  npm
  pip
  python
  rust
  cargo
  tmux
  timewarrior
)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
export LANG=en_US.UTF-8
export EDITOR="nvim"
alias vim="nvim"

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
export DEVKITPRO=/opt/devkitpro 
export DEVKITARM=$DEVKITPRO/devkitARM DEVKITPPC=$DEVKITPRO/devkitPPC MAKEFLAGS=-j8
export PATH=$DEVKITPRO/tools/bin:$DEVKITARM/bin:$DEVKITPPC/bin:$PATH:$HOME/.local/bin:/home/darkkirb/.gem/ruby/2.2.0/bin

export GTK_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=ibus

export LANG=en_US.UTF-8
export LC_CTYPE=en_US.UTF-8
export LC_NUMERIC=en_US.UTF-8
export LC_TIME=de_DE.UTF-8
export LC_COLLATE=de_DE.UTF-8
export LC_MONETARY=de_DE.UTF-8
export PKG_CONFIG_PATH=/usr/lib/pkgconfig:/usr/lib32/pkgconfig:/usr/lib64/pkgconfig:/usr/local/lib/pkgconfig:/usr/local/lib32/pkgconfig:/usr/local/lib64/pkgconfig:/usr/share/pkgconfig:/usr/local/share/pkgconfig

export XIM=ibus

export GTK_IM_MODULE=ibus

export QT_IM_MODULE=xim

export XMODIFIERS=@im=ibus

export XIM_PROGRAM="ibus-daemon"

export XIM_ARGS="--daemonize --xim"

export MPD_HOST="192.168.2.101"

alias python=python3


if [ -d ~/.cargo ]; then
    source ~/.cargo/env
fi

export PYTHON=python3.7

function run_with_timeout () { 
    local time=0.1
    if [[ $1 =~ ^[0-9]+$ ]]; then time=$1; shift; fi
    # Run in a subshell to avoid job control messages
    ( "$@" &
      child=$!
      # Avoid default notification in non-interactive shell for SIGTERM
      trap -- "" SIGTERM
      ( sleep $time
        kill $child 2> /dev/null ) &
      wait $child
    )
}

function local_network() { # returns true if my pc is available locally
    run_with_timeout ping -c 1 192.168.2.5
}

function sync-push() {
    case $(hostname) in # depending on system, we need to rsync with different addresses
        access-ark) # server, always accesses the local ip address
            addr=192.168.2.5
            ;;
        localhost)
            if local_network; then
                addr=192.168.2.5
            else
                addr=darkkirb.cf
            fi
            ;;
        *)
            addr=darkkirb.cf
            ;;
    esac
    rsync -arpve ssh --info=stats2 ~/.task darkkirb@${addr}:/home/darkkirb/
    rsync -arpve ssh --info=stats2 ~/.timewarrior/data darkkirb@${addr}:/home/darkkirb/.timewarrior/
}
function sync-pull() {
    case $(hostname) in # depending on system, we need to rsync with different addresses
        access-ark) # server, always accesses the local ip address
            addr=192.168.2.5
            ;;
        localhost)
            if local_network; then
                addr=192.168.2.5
            else
                addr=darkkirb.cf
            fi
            ;;
        *)
            addr=darkkirb.cf
            ;;
    esac
    rsync -arpve ssh --info=stats2 darkkirb@${addr}:/home/darkkirb/.task ~/
    rsync -arpve ssh --info=stats2 darkkirb@${addr}:/home/darkkirb/.timewarrior/data ~/.timewarrior
}

if [[ $=- == *i* ]]; then
    # interactive shell, show tasks if possible, synchronize if necessary
    whence task > /dev/null && task
fi
