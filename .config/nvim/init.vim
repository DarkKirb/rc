set nocompatible
filetype off
set rtp+=~/.config/nvim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'

set titlestring=vim\ %{expand(\"%t\")}
if &term =~ "^screen"
    set term=xterm-256color
    set t_ts=k
    set t_fs=\
endif
set title

set backspace=indent,eol,start
set backupdir=~/.config/nvim/backup
set directory=~/.config/nvim/swap
set writebackup
set undodir=~/.config/nvim/undo
set undofile

set history=1000
set laststatus=2
set lazyredraw
set matchtime=2
set number
set ruler
set showcmd
set showmatch
set relativenumber
set cursorline
set display=lastline,uhex

set incsearch
set ignorecase
set smartcase
set gdefault

set autoindent
set shiftwidth=4
set softtabstop=4
set expandtab
set shiftround
set smarttab
set fileformats=unix,dos
set listchars=tab:↹·,extends:⇉,precedes:⇇,nbsp:␠,trail:␠,nbsp:␣
set formatoptions=crqlj
set tabstop=4
set linebreak
set showbreak=↳\
set virtualedit=block
set mouse=a
set encoding=utf-8
setglobal fileencoding=utf-8
set nobomb
set fileencodings=utf-8

set wildmenu
set wildmode=full
set wildignore+=.*.sw*,__pycache__,*.pyc
set complete-=i
set completeopt-=preview
set autoread
set scrolloff=2
set foldmethod=indent
set foldlevel=99
set timeoutlen=1000
set ttimeoutlen=100
set nrformats-=octal
set t_Co=256
colorscheme molokai
set clipboard=unnamedplus

imap <Esc>Oq 1
imap <Esc>Or 2
imap <Esc>Os 3
imap <Esc>Ot 4
imap <Esc>Ou 5
imap <Esc>Ov 6
imap <Esc>Ow 7
imap <Esc>Ox 8
imap <Esc>Oy 9
imap <Esc>Op 0
imap <Esc>On .
imap <Esc>OR *
imap <Esc>OQ /
imap <Esc>Ol +
imap <Esc>OS -:

Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'ervandew/supertab'
Plugin 'klen/python-mode'
Plugin 'aurieh/discord.nvim'
Plugin 'posva/vim-vue'

let g:airline_powerline_fonts=1
let g:asyncrun_open=8


call vundle#end()
filetype plugin indent on

augroup Binary
	  au!
	  au BufReadPre  *.bin let &bin=1
	  au BufReadPost *.bin if &bin | %!xxd
	  au BufReadPost *.bin set ft=xxd | endif
	  au BufWritePre *.bin if &bin | %!xxd -r
	  au BufWritePre *.bin endif
	  au BufWritePost *.bin if &bin | %!xxd
	  au BufWritePost *.bin set nomod | endif
augroup END
let g:pymode_python = 'python3'
let g:tex_flavor = "latex"
