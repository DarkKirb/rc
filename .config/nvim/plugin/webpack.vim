function! s:PutLine(t)
    let t=a:t
    call append(line('$'), [t])
endfunction
function! Webpack()
    AsyncRun npm run webpack
endfunction

command! -bang -nargs=0 Webpack call Webpack()
