#!/usr/bin/env zsh

echo "Updater script..." 2>&1 | tee -a update

if [[ $EUID -eq 0 ]]; then
    echo "Performing root-exclusive updates" 2>&1 | tee -a update
    if whence pacman > /dev/null; then
        echo "Checking for pacman updates..." | tee -a update
        yes | pacman -Syu 2>&1 | tee -a update
    fi
    if whence emerge > /dev/null; then
        echo "Checking for portage updates..." | tee -a update
        whence eix-sync > /dev/null || emerge -a eix-sync 2>&1 | tee -a update
        eix-sync 2>&1 | tee -a update
        emerge -uDU @world 2>&1 | tee -a update
    fi
    for python in python2 python3 python python2.7 python3.4 python3.5 python3.6 python3.7; do
        echo "Checking for $python pip updates" | tee -a update
        if whence $python > /dev/null; then
            $python -m ensurepip 2>&1 | tee -a update
            $python -m pip install $($python -m pip freeze | awk 'BEGIN{FS="=="}{print $1}') --upgrade 2>&1 | tee -a update
        fi
    done
fi

echo "Performing non-root updates" 2>&1 | tee -a update

if whence rustup > /dev/null; then
    rustup update 2>&1 | tee -a update
fi

for python in python2 python3 python python2.7 python3.4 python3.5 python3.6 python3.7; do
    echo "Checking for $python pip updates" | tee -a update
    if whence $python > /dev/null; then
        $python -m ensurepip --user 2>&1 | tee -a update
        $python -m pip install $($python -m pip freeze | awk 'BEGIN{FS="=="}{print $1}') --upgrade --user 2>&1 | tee -a update
    fi
done

if [ "$(uname -o)" = "Android" ]; then
    echo "Doing termux updates" | tee -a update
    if whence pkg > /dev/null; then
        yes | pkg update 2>&1 | tee -a update
    fi
fi

if [ -d "$HOME/rc" ]; then
    echo "Doing RC updates" | tee -a update
    cd $HOME/rc
    git pull 2>&1 | tee -a $OLDPWD/update
    ./install.sh 2>&1 | tee -a $OLDPWD/update
    cd -
fi

if [ -d "$HOME/sources/rc" ]; then
    echo "Doing RC updates" | tee -a update
    cd $HOME/sources/rc
    git submodule update --remote 2>&1 | tee -a $OLDPWD/update
    git commit -am "Automatic update on $(date --iso)" 2>&1 | tee -a $OLDPWD/update
    git push 2>&1 | tee -a $OLDPWD/update
    ./install.sh 2>&1 | tee -a $OLDPWD/update
    cd -
fi

echo "Update completed." | tee -a update
