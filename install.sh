#!/usr/bin/env zsh
mkdir -pv .config/
if [ "$(whoami)" -ne "darkkirb" ]; then
    echo "Please change .gitignore to not contain my name and email"
fi
for f in .config/nvim .config/i3 .config/i3blocks .config/compton.conf .gitconfig .gitmessage .inputrc .oh-my-zsh .tmux.conf .xinitrc .zshrc .taskrc .zshcustom .timewarrior .i3 .Xresources; do
    ln -sfv $PWD/$f $HOME/$f
done
nvim +PluginInstall! +qall

if whence cc > /dev/null; then
    ln -sfv $PWD/ycm $HOME/.vim/bundle/YouCompleteMe
    cd ycm
    args=""
    case "$(uname -s)" in
        FreeBSD*)   args+="--clang-completer ";;
        Darwin*)    if whence xcode > /dev/null; then args+="--clang-completer "; fi;;
        *)          if whence clang > /dev/null; then args+="--clang-completer "; fi;;
    esac

    if whence mono > /dev/null; then
        args+="--cs-completer "
    fi

    if whence go > /dev/null; then
        args+="--go-completer "
    fi

    if whence npm > /dev/null; then
        if whence tsserver > /dev/null; then
        else
            if whence sudo > /dev/null; then
                sudo npm i -g typescript
            fi
        fi
    fi

    if whence rustc > /dev/null; then
        args+="--rust-completer "
    fi

    if whence javac > /dev/null; then
        args+="--java-completer "
    fi

    ./install.py ${=args}
    cd ..
    nvim +PluginInstall! +qall
fi

mkdir $HOME/.config/nvim/backup
